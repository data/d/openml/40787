# OpenML dataset: Yeast-train

https://www.openml.org/d/40787

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Source:

Creator and Maintainer: 

Kenta Nakai 
Institue of Molecular and Cellular Biology 
Osaka, University 
1-3 Yamada-oka, Suita 565 Japan 
nakai '@' imcb.osaka-u.ac.jp 
http://www.imcb.osaka-u.ac.jp/nakai/psort.html 

Donor: 

Paul Horton (paulh '@' cs.berkeley.edu)


Data Set Information:

Predicted Attribute: Localization site of protein. ( non-numeric ). 

The references below describe a predecessor to this dataset and its development. They also give results (not cross-validated) for classification by a rule-based expert system with that version of the dataset. 

Reference: &quot;Expert Sytem for Predicting Protein Localization Sites in Gram-Negative Bacteria&quot;, Kenta Nakai &amp; Minoru Kanehisa, PROTEINS: Structure, Function, and Genetics 11:95-110, 1991. 

Reference: &quot;A Knowledge Base for Predicting Protein Localization Sites in Eukaryotic Cells&quot;, Kenta Nakai &amp; Minoru Kanehisa, Genomics 14:897-911, 1992.


Attribute Information:

1. Sequence Name: Accession number for the SWISS-PROT database 
2. mcg: McGeoch's method for signal sequence recognition. 
3. gvh: von Heijne's method for signal sequence recognition. 
4. alm: Score of the ALOM membrane spanning region prediction program. 
5. mit: Score of discriminant analysis of the amino acid content of the N-terminal region (20 residues long) of mitochondrial and non-mitochondrial proteins. 
6. erl: Presence of &quot;HDEL&quot; substring (thought to act as a signal for retention in the endoplasmic reticulum lumen). Binary attribute. 
7. pox: Peroxisomal targeting signal in the C-terminus. 
8. vac: Score of discriminant analysis of the amino acid content of vacuolar and extracellular proteins. 
9. nuc: Score of discriminant analysis of nuclear localization signals of nuclear and non-nuclear proteins.

#autoxgboost #autoweka

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40787) of an [OpenML dataset](https://www.openml.org/d/40787). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40787/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40787/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40787/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

